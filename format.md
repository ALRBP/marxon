# General structure
A language description file is a YAML file. It contains a map with two kinds of elements: *contexts* and *sets*. The name of an element is its key.

# Context
A *context* corresponds to a specific situation in an input file (for example: "plain text", "string", etc). There are two special contexts: the *global context* and the *base context*. Context names starting with "$" are reserved and names starting with "_" must not be used in generic definitions (reserved for users' local uses).

## Global context
The global context, named "$glob", is a special context giving rules (*examiners*) to determine the context to start the compilation with and some parameters. It consists of a list of instruction of two kinds *examiners* and *parameters*. General considerations about contexts do not apply to the global context.

### Examiners
An examiner is a map with two keys "match" and "context". "match" is a regex (string) which will be searched in the whole document (see the **Regex** section for details on regex). "context" is the name of the context the compilation will start in if the associated regex is found in the document. The order of the examiners in the list gives their relative priority (first in list has higher priority).

### Parameters
The following parameters can be put in the global context:
- "deindent" any indentation will be removed.
- "dedoublespace" double spaces will be removed.
- "remlines" Single line breaks will be removed.

## Base context
The base context, named "$base", is the context the compilation process will start in if no global context is present or none of its rules matches (or the rules list is empty/absent). Apart from that it is a context as others (general considerations about contexts apply).

A format description must have at least a global or a base context. In the case where only the global context is present, the context given in its first rule will be the default if no rule matches.

## Type
Each context has a *type* (key "type") which is one of the following:
- passthrough: text read is preserved.
- ignore: text read is ignored.
- replace: text read is replaced by the value of the key "value".

## Rules
Each context has a list of rules (key "rules"). Which will be searched for when compiling the input. Elements of the list may be of two types: *rule* and *set*. See the **Rule** for details on Rules and the **Regex** section for details on the matching process. Sets (lists themselves) are inserted in the list of rules at the position there are mentioned.

# Set
A set is a simple list of *rules*. A sets name is its key (in the map). A set can **not** include other sets.

# Rule
A rule is a map with the following possible keys:
- match \[exactly one of "match" or "indent" is required\]: regex to match 
- indent \[exactly one of "match" or "indent" is required\]: '+' or '-', the rule will match if indentation level increases of decreases
- ignore_next\[only if match\]: number of characters at the end of the regex match to be considered out of the match (ensure the regex will never match less characters)
- ignore_prev\[only if match\]: number of characters at the beginning of the regex match to be considered out of the match (ensure the regex will never match less characters)
- action \[required\]: the action of the rule (see **Action** subsection)
- name \[required if and only if the action is "context" or "transition"\]: name of the context to enter in
- lang \[required if and only if the action is "switch"\]: name of the format the next section of the input in written in
- value \[required if and only if the action is "replace"\]: value to replace the matched text with
- replace \[only if the action is "close, "switch", "context" or "transition"\]: value to replace the matched text with (text is ignored if not present)
- sep \[only if the action is "switch", "context" or "transition"\]: whether the text of the new context is to be considered independent of the rest of the text (boolean considered True if empty)
- break \[only if the action is not "close"\]: whether the text following the instruction is to be considered independent of the rest of the text (boolean considered True if empty; sep implied if applicable)

## Action
The possible values for the "action" key are the following:
- "close": close the current context (return to previous context in stack; terminate compilation if the stack is empty)
- "switch": switch to the language specified by the "lang" key (current language (and context) is added to the stack)
- "context": switch to the context specified by the "name" key (current context is added to the stack)
- "transition": switch to the context specified by the "name" key (current context is **not** added to the stack)
- "passthrough": keep the matched text as is
- "ignore": ignore the matched text (not added to output)
- "replace": replace the matched text by the value of the "value" key in the output
- "terminate": terminate the compilation process

## Limitations
If the current context is a *replace* context, the following rules cause **undefined behavior**:
- A "switch" or "context" instruction that is not "sep", including "break" instructions
- A "passthrough", "ignore" or "replace" instruction

# Regex
Regex are standard C++ regex. They follow the ECMAScript syntax with the following restrictions:
- lookahead subpaterns are **not** allowed (their behavior in libstdc++ is unsatisfactory for this program at the time of writing; the ignore_next/prev options provide a workaround for this limitation)
- '^' and '$' are only allowed at the beginning and end of a regex respectively (they must be the first and last characters). In this position, they will behave as they should in a multiline regex (absence of multiline regex support in both libstdc++ and libc++ at the time of writing required a workaround).

All regex of a context are matched in parallel (using a single regex with '|' (or)). The selected match will be decided with the following rules (in decreasing priority order):
- The earlier begin
- The latter end (longest match; this criterion is ignored for indent rules)
- The first in the list order
