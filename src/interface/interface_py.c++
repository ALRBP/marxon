#include <tuple>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <marxon.h++>
#include "../lang/lang.h++"
#include "../compiler/compiler.h++"
#include "../config/definitions.h++"


struct World
{
    Marxon::World w;
};


void parseLangDef1(World& w, const std::unordered_map<std::string, std::string>& defMap)
{
    Marxon::parseLangDef(w.w,defMap);
}

void parseLangDef2(World& w, const std::string& defStr, std::list<std::string>& newLangs)
{
    Marxon::parseLangDef(w.w,defStr,newLangs);
}

void parseLangDef3(World& w, const std::string& defStr)
{
    Marxon::parseLangDef(w.w,defStr);
}

void parseLangDef4(World& w, const std::string& name, const std::string& def)
{
    Marxon::parseLangDef(w.w,name,def);
}

World parseLangDef5(const std::unordered_map<std::string,std::string>& defMap)
{
    return World({Marxon::parseLangDef(defMap)});
}

World parseLangDef6(const std::string& defStr)
{
    return World({Marxon::parseLangDef(defStr)});
}

World parseLangDef7(const std::string& name, const std::string& def)
{
    return World({Marxon::parseLangDef(name,def)});
}

std::string toJson(const World& w, const std::string& doc, const std::string& lang = ONLY_INDEX, const std::string& startContext = AUTO_CONTEXT, unsigned tabLen = TAB_LEN)
{
    return Marxon::toJson(w.w,doc,lang,startContext,tabLen);
}

std::string toYaml(const World& w, const std::string& doc, const std::string& lang = ONLY_INDEX, const std::string& startContext = AUTO_CONTEXT, unsigned tabLen = TAB_LEN)
{
    return Marxon::toYaml(w.w,doc,lang,startContext,tabLen);
}

std::list<std::pair<std::string,std::tuple<size_t,size_t,size_t,size_t,size_t,size_t,bool>>> toJsonList(const World& w, const std::string& doc, const std::string& lang = ONLY_INDEX, const std::string& startContext = AUTO_CONTEXT, unsigned tabLen = TAB_LEN)
{
    std::list<std::pair<std::string,std::tuple<size_t,size_t,size_t,size_t,size_t,size_t,bool>>> ans;
    std::list<Marxon::AnsPart> pa = Marxon::toJsonList(w.w,doc,lang,startContext,tabLen);
    for(const auto& p: pa)
    {
        ans.push_back({p.text,std::make_tuple(p.offset,p.offset16,p.offset32,p.len,p.len16,p.len32,p.hasText)});
    }
    return ans;
}

std::list<std::pair<std::string,std::tuple<size_t,size_t,size_t,size_t,size_t,size_t,bool>>> toYamlList(const World& w, const std::string& doc, const std::string& lang = ONLY_INDEX, const std::string& startContext = AUTO_CONTEXT, unsigned tabLen = TAB_LEN)
{
    std::list<std::pair<std::string,std::tuple<size_t,size_t,size_t,size_t,size_t,size_t,bool>>> ans;
    std::list<Marxon::AnsPart> pa = Marxon::toYamlList(w.w,doc,lang,startContext,tabLen);
    for(const auto& p: pa)
    {
        ans.push_back({p.text,std::make_tuple(p.offset,p.offset16,p.offset32,p.len,p.len16,p.len32,p.hasText)});
    }
    return ans;
}

World newWorld(void)
{
    World w({Marxon::World()});
    return w;
}

std::list<std::string> getLangs(const World& w)
{
    std::list<std::string> ans;
    for(const auto& l: w.w)
    {
        ans.push_back(l.first);
    }
    return ans;
}

void clearWorld(World& w)
{
    w.w.clear();
}

PYBIND11_MODULE(marxon_p,m)
{
    pybind11::class_<World,std::shared_ptr<World>>(m,"World");
    
    m.def("parseLangDef",&parseLangDef1);
    
    m.def("parseLangDef",&parseLangDef2);
    
    m.def("parseLangDef",&parseLangDef3);
    
    m.def("parseLangDef",&parseLangDef4);
    
    m.def("parseLangDef",&parseLangDef5);
    
    m.def("parseLangDef",&parseLangDef6);
    
    m.def("parseLangDef",&parseLangDef7);
    
    m.def("toJson",&toJson);
    
    m.def("toYaml",&toYaml);
    
    m.def("toJsonList",&toJsonList);
    
    m.def("toYamlList",&toYamlList);
    
    m.def("newWorld",&newWorld);
    
    m.def("getLangs",&getLangs);
    
    m.def("clearWorld",&clearWorld);
    
    m.attr("tabLen") = pybind11::int_(TAB_LEN);
    
    m.attr("onlyIndex") = pybind11::str(ONLY_INDEX);
    
    m.attr("autoContext") = pybind11::str(AUTO_CONTEXT);
    
    m.attr("dataPath") = pybind11::str(DATA_PATH);
}
