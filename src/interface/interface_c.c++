#include <marxon.h>
#include <marxon.h++>
#include "../lang/lang.h++"
#include "../compiler/compiler.h++"
#include "../config/definitions.h++"


struct World
{
    Marxon::World w;
};

struct Map_c
{
    std::unordered_map<std::string,std::string> m;
};

    
int MarxonInit(MarxonDefinitions* d)
{
    try
    {
        d->w = new World({Marxon::World()});
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    try
    {
        d->startContext = new Map_c;
    }
    catch(const std::exception& e)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    d->lang = (char*)calloc(strlen(ONLY_INDEX)+1,sizeof(char));
    if((!d->w) || (!d->startContext) || (!d->lang))
    {
        strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    strcpy(d->lang,ONLY_INDEX);
    d->error[0] = '\0';
    d->error[MARXON_ERR_LEN] = '\0';
    d->tabLen = TAB_LEN;
    return MARXON_SUCCESS;
}
    
int MarxonInitStr(MarxonDefinitions* d, const char* defStr)
{
    try
    {
        d->w = new World({Marxon::parseLangDef(defStr)});
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    try
    {
        d->startContext = new Map_c;
    }
    catch(const std::exception& e)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    d->lang = (char*)calloc(strlen(ONLY_INDEX)+1,sizeof(char));
    if((!d->w) || (!d->startContext) || (!d->lang))
    {
        strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    strcpy(d->lang,ONLY_INDEX);
    d->error[0] = '\0';
    d->error[MARXON_ERR_LEN] = '\0';
    d->tabLen = TAB_LEN;
    try
    {
        for(const auto& l: d->w->w)
        {
            d->startContext->m[l.first] = AUTO_CONTEXT;
        }
    }
    catch(const std::exception& e)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        if(d->w != NULL)
        {
            free(d->startContext);
        }
        if(d->w != NULL)
        {
            free(d->lang);
        }
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        if(d->w != NULL)
        {
            free(d->startContext);
        }
        if(d->w != NULL)
        {
            free(d->lang);
        }
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    return MARXON_SUCCESS;
}
    
int MarxonInitNameStr(MarxonDefinitions* d, const char* name, const char* def)
{
    try
    {
        d->w = new World({Marxon::parseLangDef(name,def)});
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    try
    {
        d->startContext = new Map_c;
    }
    catch(const std::exception& e)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    d->lang = (char*)calloc(strlen(ONLY_INDEX)+1,sizeof(char));
    if((!d->w) || (!d->startContext) || (!d->lang))
    {
        strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    strcpy(d->lang,ONLY_INDEX);
    d->error[0] = '\0';
    d->error[MARXON_ERR_LEN] = '\0';
    d->tabLen = TAB_LEN;
    try
    {
        d->startContext->m[name] = AUTO_CONTEXT;
    }
    catch(const std::exception& e)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        if(d->w != NULL)
        {
            free(d->startContext);
        }
        if(d->w != NULL)
        {
            free(d->lang);
        }
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        if(d->w != NULL)
        {
            free(d->w);
        }
        if(d->w != NULL)
        {
            free(d->startContext);
        }
        if(d->w != NULL)
        {
            free(d->lang);
        }
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    return MARXON_SUCCESS;
}
    
int MarxonAddDef(MarxonDefinitions* d, const char* defStr)
{
    std::list<std::string> newLangs;
    try
    {
        Marxon::parseLangDef(d->w->w,defStr,newLangs);
        for(const auto& l: newLangs)
        {
            d->startContext->m[l] = AUTO_CONTEXT;
        }
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    return MARXON_SUCCESS;
}
    
int MarxonAddDefName(MarxonDefinitions* d, const char* name, const char* def)
{
    try
    {
        Marxon::parseLangDef(d->w->w,name,def);
        d->startContext->m[name] = AUTO_CONTEXT;
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    return MARXON_SUCCESS;
}
    
int MarxonReset(MarxonDefinitions* d)
{
    if(d->w != NULL)
    {
        free(d->lang);
    }
    d->lang = (char*)calloc(strlen(ONLY_INDEX)+1,sizeof(char));
    if(!d->lang)
    {
        strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    d->w->w.clear();
    d->error[MARXON_ERR_LEN] = '\0';
    strcpy(d->lang,ONLY_INDEX);
    d->tabLen = TAB_LEN;
    d->startContext->m.clear();
    return MARXON_SUCCESS;
}
    
void MarxonSetTabLen(MarxonDefinitions* d, unsigned l)
{
    d->tabLen = l;
}
    
unsigned MarxonGetTabLen(MarxonDefinitions* d)
{
    return d->tabLen;
}
    
int MarxonSetLang(MarxonDefinitions* d, const char* l)
{
    if(d->w != NULL)
    {
        free(d->lang);
    }
    d->lang = (char*)calloc(strlen(l)+1,sizeof(char));
    if(!d->lang)
    {
        strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    strcpy(d->lang,l);
    return MARXON_SUCCESS;
}
    
char* MarxonGetLang(MarxonDefinitions* d)
{
    char* ans = (char*)calloc(strlen(d->lang)+1,sizeof(char));
    if(d->lang)
    {
        strcpy(ans,d->lang);
    }
    return ans;
}

int MarxonResetContext(MarxonDefinitions* d)
{
    try
    {
        d->startContext->m[d->lang] = AUTO_CONTEXT;
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    return MARXON_SUCCESS;
}
    
int MarxonResetContextLang(MarxonDefinitions* d, const char* l)
{
    try
    {
        d->startContext->m[l] = AUTO_CONTEXT;
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    return MARXON_SUCCESS;
}
    
int MarxonSetContext(MarxonDefinitions* d, const char* c)
{
    try
    {
        d->startContext->m[d->lang] = c;
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    return MARXON_SUCCESS;
}
    
int MarxonSetContextLang(MarxonDefinitions* d, const char* c, const char* l)
{
    try
    {
        d->startContext->m[l] = c;
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    return MARXON_SUCCESS;
}
    
char* MarxonGetContext(MarxonDefinitions* d)
{
    char* ans;
    try
    {
        ans = (char*)calloc(d->startContext->m[d->lang].size()+1,sizeof(char));
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return NULL;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return NULL;
    }
    if(d->lang)
    {
        try
        {
            d->startContext->m[d->lang].copy(ans,d->startContext->m[d->lang].size());
        }
        catch(const std::exception& e)
        {
            if(ans != NULL)
            {
                free(ans);
            }
            strncpy(d->error,e.what(),MARXON_ERR_LEN);
            return NULL;
        }
        catch(...)
        {
            if(ans != NULL)
            {
                free(ans);
            }
            strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
            return NULL;
        }
    }
    return ans;
}
    
char* MarxonGetContextLang(MarxonDefinitions* d, const char* l)
{
    char* ans;
    try
    {
        ans = (char*)calloc(d->startContext->m[l].size()+1,sizeof(char));
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return NULL;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return NULL;
    }
    if(d->lang)
    {
        try
        {
            d->startContext->m[l].copy(ans,d->startContext->m[l].size());
        }
        catch(const std::exception& e)
        {
            if(ans != NULL)
            {
                free(ans);
            }
            strncpy(d->error,e.what(),MARXON_ERR_LEN);
            return NULL;
        }
        catch(...)
        {
            if(ans != NULL)
            {
                free(ans);
            }
            strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
            return NULL;
        }
    }
    return ans;
}
    
bool MarxonIsSupported(MarxonDefinitions* d, const char* l)
{
    bool ans;
    try
    {
        ans = (d->startContext->m.find(l) != d->startContext->m.end());
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return false;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return false;
    }
    strncpy(d->error,"No error",MARXON_ERR_LEN);
    return ans;
}
    
char* MarxonJson(MarxonDefinitions* d, const char* doc)
{
    return MarxonJsonLangContext(d,doc,d->lang,d->startContext->m.at(d->lang).c_str());
}
    
char* MarxonJsonLang(MarxonDefinitions* d, const char* doc, const char* l)
{
    return MarxonJsonLangContext(d,doc,l,d->startContext->m.at(l).c_str());
}
    
char* MarxonYaml(MarxonDefinitions* d, const char* doc)
{
    return MarxonYamlLangContext(d,doc,d->lang,d->startContext->m.at(d->lang).c_str());
}
    
char* MarxonYamlLang(MarxonDefinitions* d, const char* doc, const char* l)
{
    return MarxonYamlLangContext(d,doc,l,d->startContext->m.at(l).c_str());
}
    
char* MarxonJsonContext(MarxonDefinitions* d, const char* doc, const char* c)
{
    return MarxonJsonLangContext(d,doc,d->lang,c);
}
    
char* MarxonJsonLangContext(MarxonDefinitions* d, const char* doc, const char* l, const char* c)
{
    std::string s;
    char* ans;
    try
    {
        s = Marxon::toJson(d->w->w,doc,l,c,d->tabLen);
        ans = (char*)calloc(s.size()+1,sizeof(char));
        if(!ans)
        {
            strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
            return NULL;
        }
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return NULL;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return NULL;
    }
    try
    {
        if(l)
        {
            s.copy(ans,s.size());
        }
    }
    catch(const std::exception& e)
    {
        if(ans != NULL)
        {
            free(ans);
        }
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return NULL;
    }
    catch(...)
    {
        if(ans != NULL)
        {
            free(ans);
        }
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return NULL;
    }
    return ans;
}
    
char* MarxonYamlContext(MarxonDefinitions* d, const char* doc, const char* c)
{
    return MarxonYamlLangContext(d,doc,d->lang,c);
}
    
char* MarxonYamlLangContext(MarxonDefinitions* d, const char* doc, const char* l, const char* c)
{
    std::string s;
    char* ans;
    try
    {
        s = Marxon::toYaml(d->w->w,doc,l,c,d->tabLen);
        ans = (char*)calloc(s.size()+1,sizeof(char));
        if(!ans)
        {
            strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
            return NULL;
        }
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return NULL;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return NULL;
    }
    try
    {
        if(d->lang)
        {
            s.copy(ans,s.size());
        }
    }
    catch(const std::exception& e)
    {
        if(ans != NULL)
        {
            free(ans);
        }
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        return NULL;
    }
    catch(...)
    {
        if(ans != NULL)
        {
            free(ans);
        }
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        return NULL;
    }
    return ans;
}

MarxonText MarxonJsonList(MarxonDefinitions* d, const char* doc)
{
    return MarxonJsonLangContextList(d,doc,d->lang,d->startContext->m.at(d->lang).c_str());
}

MarxonText MarxonJsonLangList(MarxonDefinitions* d, const char* doc, const char* l)
{
    return MarxonJsonLangContextList(d,doc,l,d->startContext->m.at(l).c_str());
}

MarxonText MarxonYamlList(MarxonDefinitions* d, const char* doc)
{
    return MarxonYamlLangContextList(d,doc,d->lang,d->startContext->m.at(d->lang).c_str());
}

MarxonText MarxonYamlLangList(MarxonDefinitions* d, const char* doc, const char* l)
{
    return MarxonYamlLangContextList(d,doc,l,d->startContext->m.at(l).c_str());
}

MarxonText MarxonJsonContextList(MarxonDefinitions* d, const char* doc, const char* c)
{
    return MarxonJsonLangContextList(d,doc,d->lang,c);
}

MarxonText MarxonJsonLangContextList(MarxonDefinitions* d, const char* doc, const char* l, const char* c)
{
    MarxonText ans;
    std::list<Marxon::AnsPart> ls;
    try
    {
        ls = Marxon::toJsonList(d->w->w,doc,l,c,d->tabLen);
        ans.len = ls.size();
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        ans.len = 0;
        ans.text = NULL;
        return ans;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        ans.len = 0;
        ans.text = NULL;
        return ans;
    }
    ans.text = (MarxonTextPart*)calloc(ans.len,sizeof(MarxonTextPart));
    if(ans.text)
    {
        int i = 0;
        for(const auto& p: ls)
        {
            try
            {
                ans.text[i].text = (char*)calloc(p.text.size()+1,sizeof(char));
                if(!ans.text[i].text)
                {
                    for(; i > 0; --i)
                    {
                        if(ans.text[i-1].text != NULL)
                        {
                            free(ans.text[i-1].text);
                        }
                    }
                    if(ans.text != NULL)
                    {
                        free(ans.text);
                    }
                    strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
                    ans.len = 0;
                    ans.text = NULL;
                    return ans;
                }
            }
            catch(const std::exception& e)
            {
                for(--i; i >= 0; --i)
                {
                    if(ans.text[i].text != NULL)
                    {
                        free(ans.text[i].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,e.what(),MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            catch(...)
            {
                for(--i; i >= 0; --i)
                {
                    if(ans.text[i].text != NULL)
                    {
                        free(ans.text[i].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            if(!ans.text[i].text)
            {
                for(; i > 0; --i)
                {
                    if(ans.text[i-1].text != NULL)
                    {
                        free(ans.text[i-1].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            try
            {
                p.text.copy(ans.text[i].text,p.text.size());
                ans.text[i].offset = p.offset;
                ans.text[i].offset16 = p.offset16;
                ans.text[i].offset32 = p.offset32;
                ans.text[i].len = p.len;
                ans.text[i].len16 = p.len16;
                ans.text[i].len32 = p.len32;
                ans.text[i].hasText = p.hasText;
            }
            catch(const std::exception& e)
            {
                for(; i >= 0; --i)
                {
                    if(ans.text[i].text != NULL)
                    {
                        free(ans.text[i].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,e.what(),MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            catch(...)
            {
                for(; i >= 0; --i)
                {
                    if(ans.text[i].text != NULL)
                    {
                        free(ans.text[i].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            ++i;
        }
    }
    else
    {
        strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
        ans.len = 0;
        ans.text = NULL;
    }
    return ans;
}

MarxonText MarxonYamlContextList(MarxonDefinitions* d, const char* doc, const char* c)
{
    return MarxonYamlLangContextList(d,doc,d->lang,c);
}

MarxonText MarxonYamlLangContextList(MarxonDefinitions* d, const char* doc, const char* l, const char* c)
{
    MarxonText ans;
    std::list<Marxon::AnsPart> ls;
    try
    {
        ls = Marxon::toYamlList(d->w->w,doc,l,c,d->tabLen);
        ans.len = ls.size();
    }
    catch(const std::exception& e)
    {
        strncpy(d->error,e.what(),MARXON_ERR_LEN);
        ans.len = 0;
        ans.text = NULL;
        return ans;
    }
    catch(...)
    {
        strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
        ans.len = 0;
        ans.text = NULL;
        return ans;
    }
    ans.text = (MarxonTextPart*)calloc(ans.len,sizeof(MarxonTextPart));
    if(ans.text)
    {
        int i = 0;
        for(const auto& p: ls)
        {
            try
            {
                ans.text[i].text = (char*)calloc(p.text.size()+1,sizeof(char));
                if(!ans.text[i].text)
                {
                    for(; i > 0; --i)
                    {
                        if(ans.text[i-1].text != NULL)
                        {
                            free(ans.text[i-1].text);
                        }
                    }
                    if(ans.text != NULL)
                    {
                        free(ans.text);
                    }
                    strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
                    ans.len = 0;
                    ans.text = NULL;
                    return ans;
                }
            }
            catch(const std::exception& e)
            {
                for(--i; i >= 0; --i)
                {
                    if(ans.text[i].text != NULL)
                    {
                        free(ans.text[i].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,e.what(),MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            catch(...)
            {
                for(--i; i >= 0; --i)
                {
                    if(ans.text[i].text != NULL)
                    {
                        free(ans.text[i].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            if(!ans.text[i].text)
            {
                for(; i > 0; --i)
                {
                    if(ans.text[i-1].text != NULL)
                    {
                        free(ans.text[i-1].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            try
            {
                p.text.copy(ans.text[i].text,p.text.size());
                ans.text[i].offset = p.offset;
                ans.text[i].offset16 = p.offset16;
                ans.text[i].offset32 = p.offset32;
                ans.text[i].len = p.len;
                ans.text[i].len16 = p.len16;
                ans.text[i].len32 = p.len32;
                ans.text[i].hasText = p.hasText;
            }
            catch(const std::exception& e)
            {
                for(; i >= 0; --i)
                {
                    if(ans.text[i].text != NULL)
                    {
                        free(ans.text[i].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,e.what(),MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            catch(...)
            {
                for(; i >= 0; --i)
                {
                    if(ans.text[i].text != NULL)
                    {
                        free(ans.text[i].text);
                    }
                }
                if(ans.text != NULL)
                {
                    free(ans.text);
                }
                strncpy(d->error,"Unknown exception",MARXON_ERR_LEN);
                ans.len = 0;
                ans.text = NULL;
                return ans;
            }
            ++i;
        }
    }
    else
    {
        strncpy(d->error,"Unable to allocate memory",MARXON_ERR_LEN);
        ans.len = 0;
        ans.text = NULL;
    }
    return ans;
}

int MarxonDefinitionsCopy(MarxonDefinitions* orig, MarxonDefinitions* dest)
{
    try
    {
        dest->tabLen = orig->tabLen;
        dest->w->w = orig->w->w;
        dest->startContext->m = orig->startContext->m;
        char* tmp = (char*)calloc(strlen(orig->lang)+1,sizeof(char));
        if(!tmp)
        {
            strncpy(dest->error,"Unable to allocate memory",MARXON_ERR_LEN);
            return MARXON_FAILURE;
        }
    }
    catch(const std::exception& e)
    {
        strncpy(dest->error,e.what(),MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    catch(...)
    {
        strncpy(dest->error,"Unknown exception",MARXON_ERR_LEN);
        return MARXON_FAILURE;
    }
    strcpy(dest->lang,orig->lang);
    return MARXON_SUCCESS;
}

void MarxonDefinitionsFree(MarxonDefinitions* d)
{
    if(d->w != NULL)
    {
        delete d->w;
    }
    if(d->startContext != NULL)
    {
        delete d->startContext;
    }
    if(d->lang != NULL)
    {
        free(d->lang);
    }
}

void MarxonTextFree(MarxonText* t)
{
    for(size_t i =0; i < t->len; ++i)
    {
        if(t->text[i].text != NULL)
        {
            free(t->text[i].text);
        }
    }
    if(t->text != NULL)
    {
        free(t->text);
    }
}

const char* MarxonDataPath(void)
{
    return DATA_PATH;
}
