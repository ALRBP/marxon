#include <marxon.h++>
#include "../lang/lang.h++"
#include "../compiler/compiler.h++"
#include "../config/definitions.h++"


namespace Marxon
{
struct World_c
{
    World w;
};
    
    
Exception::Exception(const std::string& msg) noexcept
{
    m = msg;
}

const char* Exception::what(void) const noexcept
{
    return m.c_str();
}


Definitions::Definitions(void)
{
    w = std::shared_ptr<World_c>(new World_c({World()}));
    tabLen = TAB_LEN;
    lang = ONLY_INDEX;
    startContext = std::unordered_map<std::string,std::string>();
}

Definitions::Definitions(const Definitions& d)
{
    w = std::shared_ptr<World_c>(new World_c({d.w->w}));
    tabLen = d.tabLen;
    lang = d.lang;
    startContext = d.startContext;
}

Definitions::Definitions(const std::unordered_map<std::string,std::string>& defMap)
{
    w = std::shared_ptr<World_c>(new World_c({parseLangDef(defMap)}));
    tabLen = TAB_LEN;
    lang = ONLY_INDEX;
    for(const auto& l: w->w)
    {
        startContext[l.first] = AUTO_CONTEXT;
    }
}

Definitions::Definitions(const std::string& defStr)
{
    w = std::shared_ptr<World_c>(new World_c({parseLangDef(defStr)}));
    tabLen = TAB_LEN;
    lang = ONLY_INDEX;
    for(const auto& l: w->w)
    {
        startContext[l.first] = AUTO_CONTEXT;
    }
}

Definitions::Definitions(const std::string& name, const std::string& def)
{
    w = std::shared_ptr<World_c>(new World_c({parseLangDef(name,def)}));
    tabLen = TAB_LEN;
    lang = ONLY_INDEX;
    startContext[name] = AUTO_CONTEXT;
}

Definitions& Definitions::operator=(const Definitions& d)
{
    w = std::shared_ptr<World_c>(new World_c({d.w->w}));
    tabLen = d.tabLen;
    lang = d.lang;
    startContext = d.startContext;
    return *this;
}

void Definitions::addDef(const std::unordered_map<std::string,std::string>& defMap)
{
    parseLangDef(w->w,defMap);
    for(const auto& l: defMap)
    {
        startContext[l.first] = AUTO_CONTEXT;
    }
}

void Definitions::addDef(const std::string& defStr)
{
    std::list<std::string> newLangs;
    parseLangDef(w->w,defStr,newLangs);
    for(const auto& l: newLangs)
    {
        startContext[l] = AUTO_CONTEXT;
    }
}

void Definitions::addDef(const std::string& name, const std::string& def)
{
    parseLangDef(w->w,name,def);
    startContext[name] = AUTO_CONTEXT;
}

void Definitions::reset(void) noexcept
{
    w->w.clear();
    tabLen = TAB_LEN;
    lang = ONLY_INDEX;
    startContext.clear();
}

void Definitions::setTabLen(unsigned l) noexcept
{
    tabLen = l;
}

unsigned Definitions::getTabLen(void) const noexcept
{
    return tabLen;
}

void Definitions::setLang(const std::string& l) noexcept
{
    lang = l;
}

std::string Definitions::getLang(void) const noexcept
{
    return lang;
}

void Definitions::resetContext(void)
{
    startContext[lang] = AUTO_CONTEXT;
}

void Definitions::resetContext(const std::string& l)
{
    startContext[l] = AUTO_CONTEXT;
}

void Definitions::setContext(const std::string& c)
{
    startContext[lang] = c;
}

void Definitions::setContext(const std::string& c, const std::string& l)
{
    startContext[l] = c;
}

std::string Definitions::getContext(void) const
{
    return startContext.at(lang);
}

std::string Definitions::getContext(const std::string& l) const
{
    return startContext.at(l);
}

void Definitions::setContext(const std::unordered_map<std::string,std::string>& c)
{
    for(const auto& l: c)
    {
        startContext[l.first] = l.second;
    }
}

void Definitions::replaceContext(const std::unordered_map<std::string,std::string>& c) noexcept
{
    startContext = c;
}

std::unordered_map<std::string,std::string> Definitions::getContexts(void) const
{
    return startContext;
}

bool Definitions::isSupported(const std::string& l) const
{
    return (startContext.find(l) != startContext.end());
}
    
std::string Definitions::json(const std::string& doc) const
{
    return json(doc,lang,startContext.at(lang));
}
    
std::string Definitions::json(const std::string& doc, const std::string& l) const
{
    return json(doc,l,startContext.at(l));
}
    
std::string Definitions::yaml(const std::string& doc) const
{
    return yaml(doc,lang,startContext.at(lang));
}
    
std::string Definitions::yaml(const std::string& doc, const std::string& l) const
{
    return yaml(doc,l,startContext.at(l));
}
    
std::string Definitions::jsonContext(const std::string& doc, const std::string& c) const
{
    return json(doc,lang,c);
}
    
std::string Definitions::json(const std::string& doc, const std::string& l, const std::string& c) const
{
    return toJson(w->w,doc,l,c,tabLen);
}
    
std::string Definitions::yamlContext(const std::string& doc, const std::string& c) const
{
    return yaml(doc,lang,c);
}
    
std::string Definitions::yaml(const std::string& doc, const std::string& l, const std::string& c) const
{
    return toYaml(w->w,doc,l,c,tabLen);
}
    
Text Definitions::jsonList(const std::string& doc) const
{
    return jsonList(doc,lang,startContext.at(lang));
}
    
Text Definitions::jsonList(const std::string& doc, const std::string& l) const
{
    return jsonList(doc,l,startContext.at(l));
}
    
Text Definitions::yamlList(const std::string& doc) const
{
    return yamlList(doc,lang,startContext.at(lang));
}
    
Text Definitions::yamlList(const std::string& doc, const std::string& l) const
{
    return yamlList(doc,l,startContext.at(l));
}
    
Text Definitions::jsonContextList(const std::string& doc, const std::string& c) const
{
    return jsonList(doc,lang,c);
}
    
Text Definitions::jsonList(const std::string& doc, const std::string& l, const std::string& c) const
{
    Text ans;
    std::list<AnsPart> ls = toJsonList(w->w,doc,l,c,tabLen);
    for(const auto& p: ls)
    {
        ans.push_back(TextPart({p.text,p.offset,p.offset16,p.offset32,p.len,p.len16,p.len32,p.hasText}));
    }
    return ans;
}
    
Text Definitions::yamlContextList(const std::string& doc, const std::string& c) const
{
    return yamlList(doc,lang,c);
}
    
Text Definitions::yamlList(const std::string& doc, const std::string& l, const std::string& c) const
{
    Text ans;
    std::list<AnsPart> ls = toYamlList(w->w,doc,l,c,tabLen);
    for(const auto& p: ls)
    {
        ans.push_back(TextPart({p.text,p.offset,p.offset16,p.offset32,p.len,p.len16,p.len32,p.hasText}));
    }
    return ans;
}

std::string dataPath(void)
{
    return DATA_PATH;
}
}
