#include <stack>
#include <vector>
#include <algorithm>
#include <utf8cpp/utf8.h>
#include "compiler.h++"

#define INDENT 4


namespace Marxon
{
bool hasText(const Part& p)
{
    for(const auto& e: p.elems)
    {
        if(e.type != ElementType::Control)
        {
            return true;
        }
    }
    return false;
}

std::array<size_t,3> len(const Part& p)
{
    std::string text;
    for(const auto& e: p.elems)
    {
        text+= e.text;
    }
    std::u16string text16 = utf8::utf8to16(text);
    std::u32string text32 = utf8::utf8to32(text);
    return {text.size(),text16.size(),text32.size()};
}

std::array<size_t,3> offsetConv(size_t o, const std::string& t)
{
    std::string text = t.substr(0,o);
    std::u16string text16 = utf8::utf8to16(text);
    std::u32string text32 = utf8::utf8to32(text);
    return {text.size(),text16.size(),text32.size()};
}

void to_json(nlohmann::json& j, const Element& e)
{
    switch(e.type)
    {
        case ElementType::Text:
            j["text"] = e.text;
            break;
        case ElementType::Control:
            j["markup"] = e.text;
            break;
        case ElementType::Expression:
            j["markup"] = e.text;
            j["interpretAs"] = e.eval;
            break;
    }
}

void from_json(const nlohmann::json& j, Element& e)
{
    if(j.contains("text"))
    {
        j.at("text").get_to(e.text);
        e.type = ElementType::Text;
    }
    else
    {
        j.at("markup").get_to(e.text);
        if(j.contains("interpretAs"))
        {
            j.at("interpretAs").get_to(e.eval);
            e.type = ElementType::Expression;
        }
        else
        {
            e.type = ElementType::Control;
            
        }
    }
}

void to_json(nlohmann::json& j, const Part& p)
{
    j = nlohmann::json{{"offset",p.offset},{"annotation",p.elems}};
}

void from_json(const nlohmann::json& j, Part& p)
{
    if(j.contains("offset"))
    {
        j.at("offset").get_to(p.offset);
    }
    else
    {
        p.offset = 0;
    }
}

Document compile(const World& w, const std::string& doc, const std::string& lang, const std::string& startContext, unsigned tabLen)
{
    std::list<size_t> indentPlus;
    std::list<size_t> indentMinus;
    size_t pos = 0;
    std::stack<Document> docStack;
    std::stack<std::string> langStack;
    std::stack<std::string> contextStack;
    std::stack<bool> brkStack;
    std::stack<bool> sepStack;
    std::stack<std::string::const_iterator> itStack;
    langStack.push(lang);
    Lang curLang = w.at(lang);
    std::string firstContext;
    if(startContext == AUTO_CONTEXT)
    {
        if(curLang.find(GLOBAL_CONTEXT) != curLang.end())
        {
            bool found = false;
            for(const auto& r: curLang[GLOBAL_CONTEXT].rules)
            {
                if((r.type == ActionType::Global) && (std::regex_search(doc,r.regex,std::regex_constants::match_any|std::regex_constants::match_not_null)))
                {
                    firstContext = r.text;
                    found = true;
                    break;
                }
            }
            if(!found)
            {
                if(curLang.find(BASE_CONTEXT) != curLang.end())
                {
                    firstContext = BASE_CONTEXT;
                }
                else
                {
                    firstContext = curLang[GLOBAL_CONTEXT].rules.begin()->text;
                }
            }
        }
        else
        {
            firstContext = BASE_CONTEXT;
        }
    }
    else
    {
        firstContext = startContext;
    }
    std::stack<unsigned> indentStack;
    indentStack.push(0);
    bool counting = true;
    unsigned count = 0;
    for(const auto& c: doc)
    {
        if(counting)
        {
            switch(c)
            {
                case ' ':
                    ++count;
                    break;
                case '\t':
                    count+= ((count + tabLen)/tabLen)*tabLen;
                    break;
                case '\r':
                    [[fallthrough]];
                case '\n':
                    count = 0;
                    break;
                default:
                    counting = false;
                    if(count > indentStack.top())
                    {
                        indentStack.push(count);
                        indentPlus.push_back(pos);
                    }
                    else if(count < indentStack.top())
                    {
                        while(count < indentStack.top())
                        {
                            indentStack.pop();
                            indentMinus.push_back(pos);
                        }
                        if(count != indentStack.top())//undefined behavior; indent error in source
                        {
                            indentStack.top() = count;
                        }
                    }
                    break;
            }
        }
        else
        {
            switch(c)
            {
                case '\r':
                    [[fallthrough]];
                case '\n':
                    count = 0;
                    counting = true;
                    break;
                default:
                    break;
            }
        }
        ++pos;
    }
    contextStack.push(firstContext);
    Context curContext = curLang.at(firstContext);
    brkStack.push(false);
    sepStack.push(false);
    Element newElem;
    switch(curContext.type)
    {
        case ContextType::Passthrough:
            newElem.type = ElementType::Text;
            break;
        case ContextType::Ignore:
            newElem.type = ElementType::Control;
            break;
        case ContextType::Replace:
            newElem.type = ElementType::Expression;
            newElem.eval = curContext.text;
            break;
        default:
            break;
    }
    newElem.text = "";
    Part newPart;
    newPart.offset = 0;
    newPart.elems.push_back(newElem);
    Document newDoc;
    newDoc.push_back(newPart);
    docStack.push(newDoc);
    std::string::const_iterator it = doc.begin();
    pos = 0;
    while((it != doc.end()) || (docStack.size() > 1))
    {
        size_t indentDist = std::numeric_limits<size_t>::max();
        size_t plusDist = std::numeric_limits<size_t>::max();
        size_t minusDist = std::numeric_limits<size_t>::max();
        size_t indentPrio = std::numeric_limits<size_t>::max();
        size_t plusPrio;
        size_t minusPrio;
        Rule indentRule;
        Rule plusRule;
        Rule minusRule;
        size_t indentNum = 0;
        for(const auto& r: curContext.rules)
        {
            switch(r.indent)
            {
                case IndentChange::Plus:
                    if(indentPlus.size() > 0)
                    {
                        plusDist = (indentPlus.front() - pos);
                        plusPrio = indentNum;
                        plusRule = r;
                    }
                    break;
                case IndentChange::Minus:
                    if(indentMinus.size() > 0)
                    {
                        minusDist = (indentPlus.front() - pos);
                        minusPrio = indentNum;
                        minusRule = r;
                    }
                    break;
                case IndentChange::Null:
                    break;
                default:
                    break;
            }
            ++indentNum;
        }
        if(plusDist > minusDist)
        {
            indentDist = minusDist;
            indentPrio = minusPrio;
            indentRule = minusRule;
        }
        else if(plusDist < minusDist)
        {
            indentDist = plusDist;
            indentPrio = plusPrio;
            indentRule = plusRule;
        }
        size_t regexDist = std::numeric_limits<size_t>::max();
        size_t regexPrio = std::numeric_limits<size_t>::max();
        Rule regexRule;
        std::smatch matchResult;
        Rule applyRule;
        std::string::const_iterator tokenBeg, tokenEnd;
        if(it == doc.end())
        {
            applyRule.type = ActionType::Close;
            applyRule.text2 = "";
            tokenBeg = it;
            tokenEnd = it;
        }
        else
        {
            std::regex_constants::match_flag_type mf;
            if((it != doc.begin()) && (*((it-1)) != '\n') && (*((it-1)) != '\r'))
            {
                mf = (std::regex_constants::match_not_bol|std::regex_constants::match_not_bow);
            }
            else
            {
                mf = std::regex_constants::match_default;
            }
            if(std::regex_search(it,doc.end(),matchResult,curContext.regex,mf))
            {
                size_t regexNum = 0;
                for(const auto& r: curContext.rules)
                {
                    if((r.indent == IndentChange::Null) && (std::regex_match(matchResult.begin()->first,matchResult.begin()->second,r.regex,mf)))
                    {
                        regexDist = (matchResult.begin()->first - it);
                        regexPrio = regexNum;
                        regexRule = r;
                        break;
                    }
                    ++regexNum;
                }
            }
            std::string::const_iterator secBeg, secEnd;
            if((indentDist < regexDist) || ((indentDist == regexDist) && (indentPrio < regexPrio)))
            {
                applyRule = indentRule;
                secBeg = it+indentDist;
                secEnd = it+indentDist;
            }
            else if((indentDist > regexDist) || ((indentDist == regexDist) && (indentPrio > regexPrio)))
            {
                applyRule = regexRule;
                secBeg = matchResult.begin()->first;
                secEnd = matchResult.begin()->second;
            }
            else
            {
                applyRule.type = ActionType::Terminate;
                applyRule.ignoreBeg = 0;
                applyRule.ignoreEnd = 0;
                secBeg = doc.end();
                secEnd = doc.end();
            }
            tokenBeg = (secBeg+applyRule.ignoreBeg);
            tokenEnd = (secEnd-applyRule.ignoreEnd);
            if(it != tokenBeg)
            {
                docStack.top().front().elems.back().text+= std::string(it,tokenBeg);
            }
        }
        switch(applyRule.type)
        {
            case ActionType::Close:
            {
                switch(curContext.type)
                {
                    case ContextType::Passthrough:
                    {
                        Element newElem;
                        if(applyRule.text2.size() > 0)
                        {
                            newElem.type = ElementType::Expression;
                            newElem.eval = applyRule.text2;
                        }
                        else
                        {
                            newElem.type = ElementType::Control;
                        }
                        newElem.text = std::string(tokenBeg,tokenEnd);
                        docStack.top().front().elems.push_back(newElem);
                        break;
                    }
                    case ContextType::Ignore:
                        [[fallthrough]];
                    case ContextType::Replace:
                        if(applyRule.text2.size() > 0)
                        {
                            Element newElem;
                            newElem.type = ElementType::Expression;
                            newElem.text = std::string(tokenBeg,tokenEnd);
                            newElem.eval = applyRule.text2;
                            docStack.top().front().elems.push_back(newElem);
                        }
                        else
                        {
                            docStack.top().front().elems.back().text+= std::string(tokenBeg,tokenEnd);
                        }
                        break;
                    default:
                        break;
                }
                if(contextStack.size() == 1)
                {
                    goto done;
                }
                Document dLoc = docStack.top();
                docStack.pop();
                langStack.pop();
                contextStack.pop();
                curLang = w.at(langStack.top());
                curContext = curLang[contextStack.top()];
                if(sepStack.top())
                {
                    Element newElem1, newElem2;
                    if(curContext.type == ContextType::Passthrough)
                    {
                        newElem1.type = ElementType::Control;
                        newElem1.text = "";
                        docStack.top().front().elems.push_back(newElem1);
                    }
                    if(!brkStack.top())
                    {
                        docStack.top().front().elems.back().text+= std::string(itStack.top(),tokenEnd);
                    }
                    itStack.pop();
                    if(curContext.type == ContextType::Passthrough)
                    {
                        newElem2.type = ElementType::Text;
                        newElem2.text = "";
                        docStack.top().front().elems.push_back(newElem2);
                    }
                    else
                    {
                        newElem2.type = docStack.top().front().elems.back().type;
                        newElem2.text = "";
                        newElem2.eval = "";
                        docStack.top().front().elems.push_back(newElem2);
                    }
                }
                else
                {
                    if(curContext.type == ContextType::Passthrough)
                    {
                        Element newElem;
                        newElem.type = docStack.top().front().elems.back().type;
                        newElem.text = "";
                        newElem.eval = "";
                        docStack.top().front().elems.splice(docStack.top().front().elems.end(),dLoc.front().elems);
                        docStack.top().front().elems.push_back(newElem);
                    }
                    else
                    {
                        for(const auto& e: dLoc.front().elems)
                        {
                            docStack.top().front().elems.back().text+= e.text;
                        }
                    }
                    dLoc.pop_front();
                }
                docStack.top().splice(docStack.top().end(),dLoc);
                sepStack.pop();
                if(brkStack.top())//Break instruction in a replace context is undefined behavior
                {
                    Part newPart;
                    newPart.offset = tokenEnd - doc.begin();
                    newPart.elems.push_back(docStack.top().front().elems.back());
                    docStack.top().front().elems.pop_back();
                    docStack.top().push_front(newPart);
                    
                }
                brkStack.pop();
                break;
            }
            case ActionType::Switch://non-separate Switch in a replace context is undefined behavior
                langStack.push(applyRule.text);
                contextStack.push(BASE_CONTEXT);
                curLang = w.at(applyRule.text);
                [[fallthrough]];
            case ActionType::Context://non-separate Context in a replace context is undefined behavior
            {
                sepStack.push(applyRule.sep);
                brkStack.push(applyRule.brk);
                if(applyRule.sep)
                {
                    itStack.push(tokenBeg);
                }
                if(applyRule.type == ActionType::Context)
                {
                    langStack.push(langStack.top());
                    contextStack.push(applyRule.text);
                }
                curContext = curLang[contextStack.top()];
                Element newElem, newElem2;
                switch(curContext.type)
                {
                    case ContextType::Passthrough:
                        newElem2.type = ElementType::Text;
                        newElem2.text = "";
                        [[fallthrough]];
                    case ContextType::Ignore:
                        newElem.type = ElementType::Control;
                        if(applyRule.text2.size() > 0)
                        {
                            newElem.type = ElementType::Expression;
                            newElem.eval = applyRule.text2;
                            if(curContext.type == ContextType::Ignore)
                            {
                                newElem2.type = ElementType::Control;
                                newElem2.text = "";
                            }
                        }
                        else
                        {
                            newElem.type = ElementType::Control;
                        }
                        newElem.text = std::string(tokenBeg,tokenEnd);
                        break;
                    case ContextType::Replace:
                        newElem.type = ElementType::Expression;
                        newElem.text = std::string(tokenBeg,tokenEnd);
                        newElem.eval = applyRule.text2+curContext.text;
                        break;
                    default:
                        break;
                }
                Part newPart;
                newPart.offset = tokenBeg - doc.begin();
                newPart.elems.push_back(newElem);
                if((curContext.type == ContextType::Passthrough) || ((curContext.type == ContextType::Ignore) && (applyRule.text2.size() > 0)))
                {
                    newPart.elems.push_back(newElem2);
                }
                Document newDoc;
                newDoc.push_back(newPart);
                docStack.push(newDoc);
                break;
            }
            case ActionType::Transition:
            {
                contextStack.top() = applyRule.text;
                curContext = curLang[contextStack.top()];
                Element newElem, newElem2;
                switch(curContext.type)
                {
                    case ContextType::Passthrough:
                        newElem2.type = ElementType::Text;
                        newElem2.text = "";
                        [[fallthrough]];
                    case ContextType::Ignore:
                        newElem.type = ElementType::Control;
                        if(applyRule.text2.size() > 0)
                        {
                            newElem.type = ElementType::Expression;
                            newElem.eval = applyRule.text2;
                            if(curContext.type == ContextType::Ignore)
                            {
                                newElem2.type = ElementType::Control;
                                newElem2.text = "";
                            }
                        }
                        else
                        {
                            newElem.type = ElementType::Control;
                        }
                        newElem.text = std::string(tokenBeg,tokenEnd);
                        break;
                    case ContextType::Replace:
                        newElem.type = ElementType::Expression;
                        newElem.text = std::string(tokenBeg,tokenEnd);
                        newElem.eval = applyRule.text2+curContext.text;
                        break;
                    default:
                        break;
                }
                docStack.top().front().elems.push_back(newElem);
                if(applyRule.brk)
                {
                    Part newPart;
                    newPart.offset = tokenEnd - doc.begin();
                    newPart.elems.push_back(docStack.top().front().elems.back());
                    docStack.top().front().elems.pop_back();
                    docStack.top().push_front(newPart);
                    
                }
                if((curContext.type == ContextType::Passthrough) || ((curContext.type == ContextType::Ignore) && (applyRule.text2.size() > 0)))
                {
                    docStack.top().front().elems.push_back(newElem2);
                }
                break;
            }
            case ActionType::Passthrough://Passthrough instruction in a replace context is undefined behavior
                        [[fallthrough]];
            case ActionType::Ignore://Ignore instruction in a replace context is undefined behavior
                        [[fallthrough]];
            case ActionType::Replace://Replace instruction in a replace context is undefined behavior
            {
                Element newElem1, newElem2;
                newElem2.type = docStack.top().front().elems.back().type;
                newElem2.text = "";
                newElem2.eval = "";
                if(applyRule.type == ActionType::Ignore)
                {
                    newElem1.type = ElementType::Control;
                }
                else if(applyRule.type == ActionType::Passthrough)
                {
                    newElem1.type = ElementType::Text;
                }
                else
                {
                    newElem1.type = ElementType::Expression;
                    newElem1.eval = applyRule.text;
                }
                newElem1.text = std::string(tokenBeg,tokenEnd);
                docStack.top().front().elems.push_back(newElem1);
                docStack.top().front().elems.push_back(newElem2);
                if(applyRule.brk)
                {
                    Part newPart;
                    newPart.offset = tokenEnd - doc.begin();
                    newPart.elems.push_back(docStack.top().front().elems.back());
                    docStack.top().front().elems.pop_back();
                    docStack.top().push_front(newPart);
                }
                break;
            }
            case ActionType::Terminate:
                goto done;
                break;
            default:
                break;
        }
        it = tokenEnd;
        pos = it-doc.begin();
    }
    docStack.top().front().elems.back().text+= std::string(it,doc.end());//empty stack
    done:
    bool deindent = false, dedoublespace = false, remlines = false;
    if(curLang.find(GLOBAL_CONTEXT) != curLang.end())
    {
        for(const auto& r: curLang[GLOBAL_CONTEXT].rules)
        {
            if(r.type == ActionType::Command)
            {
                if(r.text == "deindent")
                {
                    deindent = true;
                }
                else if(r.text == "dedouble_space")
                {
                    dedoublespace = true;
                }
                else if(r.text == "remlines")
                {
                    remlines = true;
                }
            }
        }
    }
    Document ltmp;
    if(deindent||dedoublespace)
    {
        for(const auto& p: docStack.top())
        {
            Part newPart;
            newPart.offset = p.offset;
            newPart.elems = std::list<Element>();
            bool init = true;
            for(const auto& e: p.elems)
            {
                switch(e.type)
                {
                    case ElementType::Text:
                    {
                        Element newElem;
                        newElem.type = ElementType::Text;
                        newElem.text = "";
                        bool prev = false, pprev = false;
                        for(const auto& c: e.text)
                        {
                            switch(c)
                            {
                                case '\t':
                                    [[fallthrough]];
                                case ' ':
                                    if((init&&deindent) && (!prev))
                                    {
                                        newPart.elems.push_back(newElem);
                                        newElem.text = "";
                                        prev = true;
                                    }
                                    else if((!(init&&deindent)) && (prev&&dedoublespace) && (!pprev))
                                    {
                                        newPart.elems.push_back(newElem);
                                        newElem.text = "";
                                        pprev = true;
                                    }
                                    newElem.text.push_back(c);
                                    prev = true;
                                    break;
                                case '\r':
                                    [[fallthrough]];
                                case '\n':
                                    if(prev&&dedoublespace)
                                    {
                                        newElem.type = ElementType::Control;
                                        newPart.elems.push_back(newElem);
                                        newElem.type = ElementType::Text;
                                        newElem.text = "";
                                    }
                                    else
                                    {
                                        newPart.elems.push_back(newElem);
                                        newElem.text = "";
                                    }
                                    init = true;
                                    prev = false;
                                    pprev = false;
                                    newElem.text.push_back(c);
                                    break;
                                default:
                                    if((pprev&&dedoublespace) || ((init&&deindent) && prev))
                                    {
                                        newElem.type = ElementType::Control;
                                        newPart.elems.push_back(newElem);
                                        newElem.type = ElementType::Text;
                                        newElem.text = "";
                                    }
                                    init = false;
                                    prev = false;
                                    pprev = false;
                                    newElem.text.push_back(c);
                                    break;
                            }
                        }
                        newPart.elems.push_back(newElem);
                        break;
                    }
                    case ElementType::Control:
                    {
                        newPart.elems.push_back(e);
                        break;
                    }
                    case ElementType::Expression:
                    {
                        Element newElem;
                        newElem.type = e.type;
                        newElem.text = e.text;
                        newElem.eval = "";
                        bool prev = false;
                        for(const auto& c: e.eval)
                        {
                            switch(c)
                            {
                                case '\t':
                                    [[fallthrough]];
                                case ' ':
                                    if(!(init&&deindent)&&!(prev&&dedoublespace))
                                    {
                                        newElem.eval.push_back(c);
                                    }
                                    prev = true;
                                    break;
                                case '\r':
                                    [[fallthrough]];
                                case '\n':
                                    init = true;
                                    prev = false;
                                    newElem.eval.push_back(c);
                                    break;
                                default:
                                    init = false;
                                    prev = false;
                                    newElem.eval.push_back(c);
                                    break;
                            }
                        }
                        newPart.elems.push_back(newElem);
                        break;
                    }
                }
            }
            ltmp.push_back(newPart);
        }
    }
    else
    {
        ltmp = docStack.top();
    }
    Document ltmp2;
    if(remlines)
    {
        for(const auto& p: ltmp)
        {
            Part newPart;
            newPart.offset = p.offset;
            newPart.elems = std::list<Element>();
            char prev = '\0';
            for(const auto& e: p.elems)
            {
                switch(e.type)
                {
                    case ElementType::Text:
                    {
                        Element newElem;
                        newElem.type = ElementType::Text;
                        newElem.text = "";
                        for(const auto& c: e.text)
                        {
                            switch(c)
                            {
                                case '\t':
                                    [[fallthrough]];
                                case ' ':
                                    newElem.text.push_back(c);
                                    break;
                                case '\r':
                                    [[fallthrough]];
                                case '\n':
                                    if(prev == c)
                                    {
                                        newElem.text.push_back(c);
                                        prev = '\0';
                                    }
                                    else
                                    {
                                        newPart.elems.push_back(newElem);
                                        newElem.type = ElementType::Control;
                                        newElem.text = c;
                                        newPart.elems.push_back(newElem);
                                        newElem.type = ElementType::Text;
                                        newElem.text = "";
                                        prev = c;
                                    }
                                    break;
                                default:
                                    prev = c;
                                    newElem.text.push_back(c);
                                    break;
                            }
                        }
                        newPart.elems.push_back(newElem);
                        break;
                    }
                    case ElementType::Control:
                    {
                        newPart.elems.push_back(e);
                        break;
                    }
                    case ElementType::Expression:
                    {
                        for(const auto& c: e.eval)
                        {
                            if((c != ' ') && (c != '\t') )
                            {
                                prev = c;
                            }
                        }
                        newPart.elems.push_back(e);
                        break;
                    }
                }
            }
            ltmp2.push_back(newPart);
        }
    }
    else
    {
        ltmp2 = ltmp;
    }
    std::vector<Part> tmp;
    tmp.reserve(ltmp2.size());
    for(const auto& p: ltmp2)
    {
        Part np;
        for(const auto& e: p.elems)
        {
            if((e.text.size() > 0) || ((e.type == ElementType::Expression)&&(e.eval.size() > 0)))
            {
                np.elems.push_back(e);
            }
        }
        if(np.elems.size() > 0)
        {
            np.offset = p.offset;
            tmp.push_back(np);
        }
    }
    std::stable_sort(tmp.begin(),tmp.end(),[](const Part& x, const Part& y){return (x.offset < y.offset);});
    Document ans = Document(tmp.begin(),tmp.end());
    return ans;
}

std::string toJson(const World& w, const std::string& doc, const std::string& lang, const std::string& startContext, unsigned tabLen)
{
    return (nlohmann::json(compile(w,doc,lang,startContext,tabLen))).dump(INDENT);
}

std::string toYaml(const World& w, const std::string& doc, const std::string& lang, const std::string& startContext, unsigned tabLen)
{
    return YAML::Dump(YAML::Node(compile(w,doc,lang,startContext,tabLen)));
}

std::list<AnsPart> toJsonList(const World& w, const std::string& doc, const std::string& lang, const std::string& startContext, unsigned tabLen)
{
    std::list<AnsPart> ans;
    Document d = compile(w,doc,lang,startContext,tabLen);
    for(const auto& p: d)
    {
        std::array<size_t,3> leng = len(p);
        std::array<size_t,3> offset = offsetConv(p.offset,doc);
        ans.push_back(AnsPart({nlohmann::json(p.elems).dump(INDENT),offset[0],offset[1],offset[2],leng[0],leng[1],leng[2],hasText(p)}));
    }
    return ans;
}

std::list<AnsPart> toYamlList(const World& w, const std::string& doc, const std::string& lang , const std::string& startContext, unsigned tabLen)
{
    std::list<AnsPart> ans;
    Document d = compile(w,doc,lang,startContext,tabLen);
    for(const auto& p: d)
    {
        std::array<size_t,3> leng = len(p);
        std::array<size_t,3> offset = offsetConv(p.offset,doc);
        ans.push_back(AnsPart({YAML::Dump(YAML::Node(p.elems)),offset[0],offset[1],offset[2],leng[0],leng[1],leng[2],hasText(p)}));
    }
    return ans;
}
}
