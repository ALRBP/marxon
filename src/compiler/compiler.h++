#ifndef MARXON_COMPIL
#define MARXON_COMPIL

#include <string>
#include <list>
#include <yaml-cpp/yaml.h>
#include <nlohmann/json.hpp>
#include <marxon.h++>
#include "../lang/lang.h++"


#define TAB_LEN 8


namespace Marxon
{
enum class ElementType {Text, Control, Expression};


struct Element
{
    ElementType type;
    std::string text;
    std::string eval;
};

struct Part
{
    size_t offset;
    std::list<Element> elems;
};

struct AnsPart
{
    std::string text;
    size_t offset;
    size_t offset16;
    size_t offset32;
    size_t len;
    size_t len16;
    size_t len32;
    bool hasText;
};


using Document = std::list<Part>;
}


namespace YAML
{
template<>
struct convert<Marxon::Element>
{
    static Node encode(const Marxon::Element& e)
    {
        Node node;
        switch(e.type)
        {
            case Marxon::ElementType::Text:
                node["text"] = e.text;
                break;
            case Marxon::ElementType::Control:
                node["markup"] = e.text;
                break;
            case Marxon::ElementType::Expression:
                node["markup"] = e.text;
                node["interpretAs"] = e.eval;
                break;
        }
        return node;
    }

    static bool decode(const Node& node, Marxon::Element& e)
    {
        if(node["text"])
        {
            e.type = Marxon::ElementType::Text;
            e.text = node["text"].as<std::string>();
        }
        else
        {
            e.text = node["text"].as<std::string>();
            if(node["interpretAs"])
            {
                e.type = Marxon::ElementType::Expression;
                e.eval = node["interpretAs"].as<std::string>();
            }
            else
            {
                e.type = Marxon::ElementType::Control;
            }
        }
        return true;
    }
};

template<>
struct convert<Marxon::Part>
{
    static Node encode(const Marxon::Part& p)
    {
        Node node;
        node["offset"] = p.offset;
        node["annotation"] = p.elems;
        return node;
    }

    static bool decode(const Node& node, Marxon::Part& p)
    {
        p.elems = node["annotation"].as<std::list<Marxon::Element>>();
        if(node["offset"])
        {
            p.offset = node["offset"].as<size_t>();
        }
        else
        {
            p.offset = 0;
        }
        return true;
    }
};
}


namespace Marxon
{
std::string toJson(const World& w, const std::string& doc, const std::string& lang = ONLY_INDEX, const std::string& startContext = AUTO_CONTEXT, unsigned tabLen = TAB_LEN);

std::string toYaml(const World& w, const std::string& doc, const std::string& lang = ONLY_INDEX, const std::string& startContext = AUTO_CONTEXT, unsigned tabLen = TAB_LEN);

std::list<AnsPart> toJsonList(const World& w, const std::string& doc, const std::string& lang = ONLY_INDEX, const std::string& startContext = AUTO_CONTEXT, unsigned tabLen = TAB_LEN);

std::list<AnsPart> toYamlList(const World& w, const std::string& doc, const std::string& lang = ONLY_INDEX, const std::string& startContext = AUTO_CONTEXT, unsigned tabLen = TAB_LEN);

}
#endif
