#ifndef MARXON_C_H
#define MARXON_C_H

#include<stdlib.h>

#define MARXON_SUCCESS EXIT_SUCCESS
#define MARXON_FAILURE EXIT_FAILURE
#define MARXON_ERR_LEN 1000


extern "C"
{
struct World;

struct Map_c;


typedef struct
{
    World* w;
    unsigned tabLen;
    char* lang;
    Map_c* startContext;
    char error[MARXON_ERR_LEN+1];
} MarxonDefinitions;

typedef struct
{
    char* text;
    size_t offset;
    size_t offset16;
    size_t offset32;
    size_t len;
    size_t len16;
    size_t len32;
    bool hasText;
} MarxonTextPart;

typedef struct
{
    MarxonTextPart* text;
    size_t len;
} MarxonText;


int MarxonInit(MarxonDefinitions* d);
    
int MarxonInitStr(MarxonDefinitions* d, const char* defStr);
    
int MarxonInitNameStr(MarxonDefinitions* d, const char* name, const char* def);
    
int MarxonAddDef(MarxonDefinitions* d, const char* defStr);
    
int MarxonAddDefName(MarxonDefinitions* d, const char* name, const char* def);
    
int MarxonReset(MarxonDefinitions* d);
    
void MarxonSetTabLen(MarxonDefinitions* d, unsigned l);
    
unsigned MarxonGetTabLen(const MarxonDefinitions* d);
    
int MarxonSetLang(MarxonDefinitions* d, const char* l);
    
char* MarxonGetLang(const MarxonDefinitions* d);

int MarxonResetContext(MarxonDefinitions* d);
    
int MarxonResetContextLang(MarxonDefinitions* d, const char* l);
    
int MarxonSetContext(MarxonDefinitions* d, const char* c);
    
int MarxonSetContextLang(MarxonDefinitions* d, const char* c, const char* l);
    
char* MarxonGetContext(MarxonDefinitions* d);
    
char* MarxonGetContextLang(MarxonDefinitions* d, const char* l);

bool MarxonIsSupported(MarxonDefinitions* d, const char* l);
    
char* MarxonJson(MarxonDefinitions* d, const char* doc);

char* MarxonJsonLang(MarxonDefinitions* d, const char* doc, const char* l);
    
char* MarxonYaml(MarxonDefinitions* d, const char* doc);

char* MarxonYamlLang(MarxonDefinitions* d, const char* doc, const char* l);
    
char* MarxonJsonContext(MarxonDefinitions* d, const char* doc, const char* c);

char* MarxonJsonLangContext(MarxonDefinitions* d, const char* doc, const char* l, const char* c);

char* MarxonYamlContext(MarxonDefinitions* d, const char* doc, const char* c);

char* MarxonYamlLangContext(MarxonDefinitions* d, const char* doc, const char* l, const char* c);

MarxonText MarxonJsonList(MarxonDefinitions* d, const char* doc);

MarxonText MarxonJsonLangList(MarxonDefinitions* d, const char* doc, const char* l);

MarxonText MarxonYamlList(MarxonDefinitions* d, const char* doc);

MarxonText MarxonYamlLangList(MarxonDefinitions* d, const char* doc, const char* l);

MarxonText MarxonJsonContextList(MarxonDefinitions* d, const char* doc, const char* c);

MarxonText MarxonJsonLangContextList(MarxonDefinitions* d, const char* doc, const char* l, const char* c);

MarxonText MarxonYamlContextList(MarxonDefinitions* d, const char* doc, const char* c);

MarxonText MarxonYamlLangContextList(MarxonDefinitions* d, const char* doc, const char* l, const char* c);

int MarxonDefinitionsCopy(MarxonDefinitions* orig, MarxonDefinitions* dest);

void MarxonDefinitionsFree(MarxonDefinitions* d);

void MarxonTextFree(MarxonText* t);

const char* MarxonDataPath(void);
}
#endif
