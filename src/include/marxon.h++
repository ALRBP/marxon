#ifndef MARXON_H
#define MARXON_H

#include <memory>
#include <string>
#include <list>
#include <unordered_map>
#include <exception>

namespace Marxon
{
struct TextPart
{
    std::string text;
    size_t offset;
    size_t offset16;
    size_t offset32;
    size_t len;
    size_t len16;
    size_t len32;
    bool hasText;
};

struct World_c;


using Text = std::list<TextPart>;


class Exception: public std::exception
{
public:
    Exception(void) = default;
    
    Exception(const Exception& e) = default;
    
    explicit Exception(const std::string& msg) noexcept;
    
    ~Exception(void) = default;
    
    Exception& operator=(const Exception& e) = default;
    
    const char* what(void) const noexcept override;

private:
    std::string m;
};

class Definitions
{
public:
    Definitions(void);
    
    Definitions(const Definitions& d);
    
    explicit Definitions(const std::unordered_map<std::string,std::string>& defMap);
    
    explicit Definitions(const std::string& defStr);
    
    explicit Definitions(const std::string& name, const std::string& def);

    ~Definitions(void) = default;
    
    Definitions& operator=(const Definitions& d);
    
    void addDef(const std::unordered_map<std::string,std::string>& defMap);
    
    void addDef(const std::string& defStr);
    
    void addDef(const std::string& name, const std::string& def);
    
    void reset(void) noexcept;
    
    void setTabLen(unsigned l) noexcept;

    unsigned getTabLen(void) const noexcept;

    void setLang(const std::string& l) noexcept;

    std::string getLang(void) const noexcept;

    void resetContext(void);

    void resetContext(const std::string& l);

    void setContext(const std::string& c);

    void setContext(const std::string& c, const std::string& l);

    std::string getContext(void) const;

    std::string getContext(const std::string& l) const;

    void setContext(const std::unordered_map<std::string,std::string>& c);

    void replaceContext(const std::unordered_map<std::string,std::string>& c) noexcept;

    std::unordered_map<std::string,std::string> getContexts(void) const;
    
    bool isSupported(const std::string& l) const;
    
    std::string json(const std::string& doc) const;
    
    std::string json(const std::string& doc, const std::string& l) const;

    std::string yaml(const std::string& doc) const;
    
    std::string yaml(const std::string& doc, const std::string& l) const;
    
    std::string jsonContext(const std::string& doc, const std::string& c) const;
    
    std::string json(const std::string& doc, const std::string& l, const std::string& c) const;

    std::string yamlContext(const std::string& doc, const std::string& c) const;
    
    std::string yaml(const std::string& doc, const std::string& l, const std::string& c) const;

    Text jsonList(const std::string& doc) const;
    
    Text jsonList(const std::string& doc, const std::string& l) const;

    Text yamlList(const std::string& doc) const;
    
    Text yamlList(const std::string& doc, const std::string& l) const;

    Text jsonContextList(const std::string& doc, const std::string& c) const;
    
    Text jsonList(const std::string& doc, const std::string& l, const std::string& c) const;

    Text yamlContextList(const std::string& doc, const std::string& c) const;
    
    Text yamlList(const std::string& doc, const std::string& l, const std::string& c) const;

private:
    std::shared_ptr<World_c> w;
    
    unsigned tabLen;
    
    std::string lang;
    
    std::unordered_map<std::string,std::string> startContext;
};

std::string dataPath(void);
}
#endif
