import marxon_p as mp

class TextPart:
    def __init__(self):
        self.text = ""
        self.offset = 0
        self.offset16 = 0
        self.offset32 = 0
        self.leng = 0
        self.leng16 = 0
        self.leng32 = 0
        self.hasText = False

class Definitions:
    def __init__(self,defs=None,name=None):
        self.__tabLen = mp.tabLen
        self.__lang = mp.onlyIndex
        self.__startContext = {}
        if defs != None:
            if name != None:
                self.__w = mp.parseLangDef(name,defs)
                self.__startContext[name] = mp.autoContext
            else:
                self.__w = mp.parseLangDef(defs)
                ls = mp.getLangs(self.__w)
                for l in ls:
                    self.__startContext[l] = mp.autoContext
        else:
            self.__w = mp.newWorld()
            
    def addDef(self,defs=None,name=None):
        if name != None:
            mp.parseLangDef(self.__w,name,defs)
            self.__startContext[name] = mp.autoContext
        else:
            if isinstance(defs,dict):
                mp.parseLangDef(self.__w,defs)
                for l in defs.keys():
                    self.__startContext[l] = mp.autoContext
            else:
                nl = []
                mp.parseLangDef(self.__w,defs,nl)
                for l in nl:
                    self.__startContext[l] = mp.autoContext
          
    def reset(self):
        mp.clearWorld(self.__w)
        self.__tabLen = mp.tabLen
        self.__lang = mp.onlyIndex
        self.__startContext.clear()
        
    def setTabLen(self,l):
        self.__tabLen = l
        
    def getTabLen(self):
        return self.__tabLen
        
    def setLang(self,l):
        self.__lang = l
        
    def getLang(self):
        return self.__lang
          
    def resetContext(self,l=None):
        if l == None:
            l = self.__lang
        self.__startContext[l] = mp.autoContext
          
    def setContext(self,c,l=None):
        if l == None:
            if isinstance(c,dict):
                self.__startContext.update(c) 
            else:
                self.__startContext[self.__lang] = c
        else:
            self.__startContext[l] = c
          
    def getContext(self,l=None):
        if l == None:
            l = self.__lang
        return self.__startContext[l]
          
    def replaceContext(self,c):
        self.__startContext = c
          
    def getContexts(self):
        return self.__startContext
    
    def isSupported(self,l):
        return (l in self.__startContext)
    
    def json(self,doc,l=None,c=None):
        if l == None:
            l = self.__lang
        if c == None:
            c = self.__startContext[l]
        return mp.toJson(self.__w,doc,l,c,self.__tabLen)
    
    def yaml(self,doc,l=None,c=None):
        if l == None:
            l = self.__lang
        if c == None:
            c = self.__startContext[l]
        return mp.toYaml(self.__w,doc,l,c,self.__tabLen)
    
    def jsonList(self,doc,l=None,c=None):
        if l == None:
            l = self.__lang
        if c == None:
            c = self.__startContext[l]
        ls = mp.toJsonList(self.__w,doc,l,c,self.__tabLen)
        ans = []
        for e in ls:
            p = TextPart()
            p.text = e[0]
            p.offset = e[1][0]
            p.offset16 = e[1][1]
            p.offset32 = e[1][2]
            p.leng = e[1][3]
            p.leng16 = e[1][4]
            p.leng32 = e[1][5]
            p.hasText = e[1][6]
            ans.append(p)
        return ans
    
    def yamlList(self,doc,l=None,c=None):
        if l == None:
            l = self.__lang
        if c == None:
            c = self.__startContext[l]
        ls = mp.toYamlList(self.__w,doc,l,c,self.__tabLen)
        ans = []
        for e in ls:
            p = TextPart()
            p.text = e[0]
            p.offset = e[1][0]
            p.offset16 = e[1][1]
            p.offset32 = e[1][2]
            p.leng = e[1][3]
            p.leng16 = e[1][4]
            p.leng32 = e[1][5]
            p.hasText = e[1][6]
            ans.append(p)
        return ans
          
def dataPath():
    return mp.dataPath
