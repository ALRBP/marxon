#ifndef MAXSON_LANG
#define MAXSON_LANG

#include <string>
#include <list>
#include <unordered_map>
#include <regex>
#include <yaml-cpp/yaml.h>
#include <marxon.h++>
#include "../utils/utils.h++"


#define ONLY_INDEX "$only"
#define BASE_CONTEXT "$base"
#define GLOBAL_CONTEXT "$glob"
#define AUTO_CONTEXT "$auto"


namespace Marxon
{
enum class ActionType {Close, Switch, Context, Transition, Passthrough, Ignore, Replace, Terminate, Global, Set, Command};

enum class ContextType {Passthrough, Ignore, Replace, Global, Set};

enum class IndentChange {Null, Plus, Minus};


struct Rule
{
    std::string match;
    unsigned ignoreEnd;
    unsigned ignoreBeg;
    IndentChange indent;
    ActionType type;
    std::string text;
    std::string text2;
    bool sep;
    bool brk;
    std::regex regex;
};

struct Context
{
    ContextType type;
    std::list<Rule> rules;
    std::string text;
    std::regex regex;
};


using Lang = std::unordered_map<std::string,Context>;

using World = std::unordered_map<std::string,Lang>;
}


namespace YAML
{
template<>
struct convert<Marxon::Rule>
{
    static Node encode(const Marxon::Rule& r)
    {
        Node node;
        switch(r.indent)
        {
            case Marxon::IndentChange::Null:
                if(r.text.size() > 0)
                {
                    node["match"] = r.match;
                }
                if(r.ignoreEnd != 0)
                {
                    node["ignore_end"] = r.ignoreEnd;
                }
                if(r.ignoreBeg != 0)
                {
                    node["ignore_begin"] = r.ignoreBeg;
                }
                break;
            case Marxon::IndentChange::Plus:
                node["indent"] = "+";
                break;
            case Marxon::IndentChange::Minus:
                node["indent"] = "-";
                break;
            default:
                break;
        }
        switch(r.type)
        {
            case Marxon::ActionType::Close:
                node["action"] = "close";
                if(r.text2.size() > 0)
                {
                    node["replace"] = r.text2;
                }
                break;
            case Marxon::ActionType::Switch:
                node["action"] = "switch";
                node["lang"] = r.text;
                if(r.text2.size() > 0)
                {
                    node["replace"] = r.text2;
                }
                break;
            case Marxon::ActionType::Context:
                node["action"] = "context";
                node["name"] = r.text;
                if(r.text2.size() > 0)
                {
                    node["replace"] = r.text2;
                }
                break;
            case Marxon::ActionType::Transition:
                node["action"] = "transition";
                node["name"] = r.text;
                if(r.text2.size() > 0)
                {
                    node["replace"] = r.text2;
                }
                break;
            case Marxon::ActionType::Passthrough:
                node["action"] = "passthrough";
                break;
            case Marxon::ActionType::Ignore:
                node["action"] = "ignore";
                break;
            case Marxon::ActionType::Replace:
                node["action"] = "replace";
                node["value"] = r.text;
                break;
            case Marxon::ActionType::Terminate:
                node["action"] = "terminate";
                if(r.text2.size() > 0)
                {
                    node["replace"] = r.text2;
                }
                break;
            case Marxon::ActionType::Global:
                node["context"] = r.text;
                break;
            case Marxon::ActionType::Command:
                node = r.text;
                break;
            default:
                break;
        }
        if(r.brk)
        {
            node["break"] = Null;
        }
        else if(r.sep)
        {
            node["separate"] = Null;
        }
        return node;
    }

    static bool decode(const Node& node, Marxon::Rule& r)
    {
        if(!node.IsMap())
        {
            r.type = Marxon::ActionType::Command;
            r.text = node.as<std::string>();
            r.brk = false;
            r.sep = false;
            return true;
        }
        if(node["indent"])
        {
            char val = node["indent"].as<char>();
            switch(val)
            {
                case '+':
                    r.indent = Marxon::IndentChange::Plus;
                    break;
                case '-':
                    r.indent = Marxon::IndentChange::Minus;
                    break;
                default:
                    throw Marxon::Exception("Invalid value for key \"indent\" when parsing rule");
                    break;
            }
            r.ignoreEnd = 0;
            r.ignoreBeg = 0;
            r.match = "";
        }
        else if(node["set"])
        {
            r.type = Marxon::ActionType::Set;
            r.text = node["set"].as<std::string>();
        }
        else
        {
            r.match = node["match"].as<std::string>();
            if(node["ignore_end"])
            {
                r.ignoreEnd = node["ignore_end"].as<unsigned>();
            }
            else
            {
                r.ignoreEnd = 0;
            }
            if(node["ignore_begin"])
            {
                r.ignoreBeg = node["ignore_begin"].as<unsigned>();
            }
            else
            {
                r.ignoreBeg = 0;
            }
            r.indent = Marxon::IndentChange::Null;
            r.regex = std::regex(r.match,std::regex::optimize);
        }
        if(node["action"])
        {
            std::string act = node["action"].as<std::string>();
            if(act == "close")
            {
                r.type = Marxon::ActionType::Close;
            }
            else if(act == "switch")
            {
                r.type = Marxon::ActionType::Switch;
                r.text = node["lang"].as<std::string>();
            }
            else if(act == "context")
            {
                r.type = Marxon::ActionType::Context;
                r.text = node["name"].as<std::string>();
            }
            else if(act == "transition")
            {
                r.type = Marxon::ActionType::Transition;
                r.text = node["name"].as<std::string>();
            }
            else if(act == "passthrough")
            {
                r.type = Marxon::ActionType::Passthrough;
            }
            else if(act == "ignore")
            {
                r.type = Marxon::ActionType::Ignore;
            }
            else if(act == "replace")
            {
                r.type = Marxon::ActionType::Replace;
                r.text = node["value"].as<std::string>();
            }
            else if(act == "terminate")
            {
                r.type = Marxon::ActionType::Terminate;
            }
            else
            {
                throw Marxon::Exception("Invalid value for key \"action\" when parsing rule");
            }
            if(node["break"] && !(node["break"].IsScalar() && !(node["break"].as<bool>())))
            {
                r.brk = true;
                r.sep = true;
            }
            else if(node["separate"] && !(node["separate"].IsScalar() && !(node["separate"].as<bool>())))
            {
                r.brk = false;
                r.sep = true;
            }
            else
            {
                r.brk = false;
                r.sep = false;
            }
        }
        else if(node["context"])
        {
            r.type = Marxon::ActionType::Global;
            r.text = node["context"].as<std::string>();
        }
        else
        {
            r.brk = false;
            r.sep = false;
        }
        if(node["replace"])
        {
            r.text2 = node["replace"].as<std::string>();
        }
        else
        {
            r.text2 = "";
        }
        return true;
    }
};

template<>
struct convert<Marxon::Context>
{
    static Node encode(const Marxon::Context& c)
    {
        Node node;
        switch(c.type)
        {
            case Marxon::ContextType::Passthrough:
                node["type"] = "passthrough";
                break;
            case Marxon::ContextType::Ignore:
                node["type"] = "ignore";
                break;
            case Marxon::ContextType::Replace:
                node["type"] = "replace";
                node["value"] = c.text;
                break;
            case Marxon::ContextType::Global:
                node = c.rules;
                break;
            default:
                break;
        }
        if(c.type != Marxon::ContextType::Global)
        {
            node["rules"] = c.rules;
        }
        return node;
    }

    static bool decode(const Node& node, Marxon::Context& c)
    {
        if(node.IsMap())
        {
            std::string type = node["type"].as<std::string>();
            if(type == "passthrough")
            {
                c.type = Marxon::ContextType::Passthrough;
            }
            else if(type == "ignore")
            {
                c.type = Marxon::ContextType::Ignore;
            }
            else if(type == "replace")
            {
                c.type = Marxon::ContextType::Replace;
                c.text = node["value"].as<std::string>();
            }
            else
            {
                throw Marxon::Exception("Invalid value for key \"type\" when parsing context");
            }
            if(node["rules"])
            {
                c.rules = node["rules"].as<std::list<Marxon::Rule>>();
            }
        }
        else
        {
            if(node.size() == 0)
            {
                throw Marxon::Exception("Empty context");
            }
            c.rules = node.as<std::list<Marxon::Rule>>();
            if(c.rules.begin()->type == Marxon::ActionType::Global)
            {
                c.type = Marxon::ContextType::Global;
            }
            else
            {
                c.type = Marxon::ContextType::Set;
            }
        }
        return true;
    }
};
}


namespace Marxon
{
void parseLangDef(World& w, const std::unordered_map<std::string, std::string>& defMap);

void parseLangDef(World& w, const std::string& defStr, std::list<std::string>& newLangs);

void parseLangDef(World& w, const std::string& defStr);

void parseLangDef(World& w, const std::string& name, const std::string& def);

World parseLangDef(const std::unordered_map<std::string,std::string>& defMap);

World parseLangDef(const std::string& defStr);

World parseLangDef(const std::string& name, const std::string& def);
}
#endif
