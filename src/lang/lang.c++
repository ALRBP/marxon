#include "lang.h++"


namespace Marxon
{

bool isLastEol(const std::string rx)
{
    bool esc = false, ans = false;
    for(const auto& c: rx)
    {
        ans = false;
        if(esc)
        {
            esc = false;
        }
        else
        {
            switch(c)
            {
                case '\\':
                    esc = true;
                    break;
                case '$':
                    ans = true;
                    break;
                default:
                    break;
            }
        }
    }
    return ans;
}

Lang postProcessLang(const Lang& l)
{
    Lang lt, ans;
    for(const auto& c: l)//Workaround for lack of multiline regex support in libstdc++/libc++
    {
        Context ct;
        ct.type = c.second.type;
        ct.text = c.second.text;
        ct.rules = std::list<Rule>();
        for(const auto& r: c.second.rules)
        {
            ct.rules.push_back(r);
            if(r.match.size() > 0)
            {
                if(r.match.front() == '^')
                {
                    Rule rt;
                    rt.type = r.type;
                    rt.text = r.text;
                    rt.text2 = r.text2;
                    rt.sep = r.sep;
                    rt.brk = r.brk;
                    rt.indent = IndentChange::Null;
                    rt.ignoreEnd = r.ignoreEnd;
                    rt.ignoreBeg = r.ignoreBeg+1;
                    rt.match = "(\\n|\\r)";
                    rt.match.append(r.match);
                    rt.match.erase(7,1);
                    rt.regex = std::regex(rt.match,std::regex::optimize);
                    ct.rules.push_back(rt);
                    if(isLastEol(r.match))
                    {
                        Rule rt2, rt3;
                        rt2.type = r.type;
                        rt2.text = r.text;
                        rt2.text2 = r.text2;
                        rt2.sep = r.sep;
                        rt2.brk = r.brk;
                        rt2.indent = IndentChange::Null;
                        rt2.ignoreEnd = r.ignoreEnd+1;
                        rt2.ignoreBeg = r.ignoreBeg;
                        rt2.match = r.match;
                        rt2.match.pop_back();
                        rt2.match.append("(\\n|\\r)");
                        rt2.regex = std::regex(rt2.match,std::regex::optimize);
                        ct.rules.push_back(rt2);
                        rt3.type = r.type;
                        rt3.text = r.text;
                        rt3.text2 = r.text2;
                        rt3.sep = r.sep;
                        rt3.brk = r.brk;
                        rt3.indent = IndentChange::Null;
                        rt3.ignoreEnd = r.ignoreEnd+1;
                        rt3.ignoreBeg = r.ignoreBeg+1;
                        rt3.match = "(\\n|\\r)";
                        rt3.match.append(r.match);
                        rt3.match.erase(7,1);
                        rt3.match.pop_back();
                        rt3.match.append("(\\n|\\r)");
                        rt3.regex = std::regex(rt3.match,std::regex::optimize);
                        ct.rules.push_back(rt3);
                    }
                }
                else if(isLastEol(r.match))
                {
                    Rule rt;
                    rt.type = r.type;
                    rt.text = r.text;
                    rt.text2 = r.text2;
                    rt.sep = r.sep;
                    rt.brk = r.brk;
                    rt.indent = IndentChange::Null;
                    rt.ignoreEnd = r.ignoreEnd+1;
                    rt.ignoreBeg = r.ignoreBeg;
                    rt.match = r.match;
                    rt.match.pop_back();
                    rt.match.append("(\\n|\\r)");
                    rt.regex = std::regex(rt.match,std::regex::optimize);
                    ct.rules.push_back(rt);
                }
            }
        }
        lt[c.first] = ct;
    }
    std::unordered_map<std::string,std::list<Rule>> sets;
    for(const auto& c: lt)
    {
        if(c.second.type == ContextType::Set)
        {
            sets[c.first] = c.second.rules;
        }
        else
        {
            ans[c.first] = c.second;
        }
    }
    for(auto& c: ans)
    {
        std::list<Rule>::const_iterator it = c.second.rules.begin();
        for(const auto& r: c.second.rules)
        {
            if(r.type == ActionType::Set)
            {
                c.second.rules.splice(it,std::list(sets[r.text]));
            }
            ++it;
        }
        std::list<Rule> tmp;
        for(const auto& r: c.second.rules)
        {
            if(r.type != ActionType::Set)
            {
                tmp.push_back(r);
            }
        }
        c.second.rules = tmp;
        std::string reg = "(\\0)";
        for(const auto& r: c.second.rules)
        {
            if(r.indent == IndentChange::Null)
            {
                reg+= ("|("+r.match+")");
            }
        }
        c.second.regex = std::regex(reg,std::regex::optimize);
    }
    return ans;
}

void parseLangDef(World& w, const std::unordered_map<std::string,std::string>& defMap)
{
    for(const auto& d: defMap)
    {
        YAML::Node def = YAML::Load(d.second);
        if(!def.IsNull())
        {
            w[d.first] = def.as<Lang>();
            if((w[d.first].find(BASE_CONTEXT) == w[d.first].end()) && (w[d.first].find(GLOBAL_CONTEXT) == w[d.first].end()))
            {
                throw Exception("No base nor global context declared for language \""+d.first+"\". Please declare a \""+BASE_CONTEXT+"\" or a \""+GLOBAL_CONTEXT+"\" context");
            }
            w[d.first] = postProcessLang(w[d.first]);
        }
    }
}

void parseLangDef(World& w, const std::string& defStr, std::list<std::string>& newLangs)
{
    YAML::Node def = YAML::Load(defStr);
    if(def.IsNull())
    {
        return;
    }
    if(!def.IsMap() || (!(def.begin()->second.IsMap() || def.begin()->second.IsSequence())))
    {
        throw Exception("This does not seems to be a valid Marxon language description");
    }
    if(def.begin()->second.IsSequence() || (def.begin()->second["type"] && def.begin()->second["type"].IsScalar()))
    {
        w[ONLY_INDEX] = def.as<Lang>();
        if((w[ONLY_INDEX].find(BASE_CONTEXT) == w[ONLY_INDEX].end()) && (w[ONLY_INDEX].find(GLOBAL_CONTEXT) == w[ONLY_INDEX].end()))
        {
            throw Exception(std::string("No base nor global context declared. Please declare a \"")+BASE_CONTEXT+"\" or a \""+GLOBAL_CONTEXT+"\" context");
        }
        w[ONLY_INDEX] = postProcessLang(w[ONLY_INDEX]);
        newLangs.push_back(ONLY_INDEX);
    }
    else
    {
        if(w.empty())
        {
            w = def.as<World>();
            for(auto& l: w)
            {
                if((l.second.find(BASE_CONTEXT) == l.second.end()) && (l.second.find(GLOBAL_CONTEXT) == l.second.end()))
                {
                    throw Exception("No base nor global context declared for language \""+l.first+"\". Please declare a \""+BASE_CONTEXT+"\" or a \""+GLOBAL_CONTEXT+"\" context");
                }
                w[l.first] = postProcessLang(l.second);
                newLangs.push_back(l.first);
            }
        }
        else
        {
            World nw = def.as<World>();
            for(auto& l: nw)
            {
                if((l.second.find(BASE_CONTEXT) == l.second.end()) && (l.second.find(GLOBAL_CONTEXT) == l.second.end()))
                {
                    throw Exception("No base nor global context declared for language \""+l.first+"\". Please declare a \""+BASE_CONTEXT+"\" or a \""+GLOBAL_CONTEXT+"\" context");
                }
                w[l.first] = postProcessLang(l.second);
                newLangs.push_back(l.first);
            }
        }
    }
}

void parseLangDef(World& w, const std::string& defStr)
{
    std::list<std::string> l;
    parseLangDef(w,defStr,l);
}

void parseLangDef(World& w, const std::string& name, const std::string& defStr)
{
    YAML::Node def = YAML::Load(defStr);
    w[name] = postProcessLang(def.as<Lang>());
}

World parseLangDef(const std::unordered_map<std::string,std::string>& defMap)
{
    World w;
    parseLangDef(w,defMap);
    return w;
}

World parseLangDef(const std::string& defStr)
{
    World w;
    parseLangDef(w,defStr);
    return w;
}

World parseLangDef(const std::string& name, const std::string& def)
{
    World w;
    parseLangDef(w,name,def);
    return w;
}
}
