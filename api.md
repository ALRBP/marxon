# General considerations
Marxon is a C++ library and provides additional Python (**alpha**) and C (**untested**, only for experimentation) bindings.

Marxon uses semantic versioning (x.y.z). API breaks can only occur on "x" releases and ABI breaks can only occur on "y" releases, except if "x" is 0. In that case, API can break on every "y" release and ABI can break on every "z" release.

All text **must** be **utf8**-encoded. Be cautious when working with non-**utf8** libraries (like Qt). Ensure to properly convert text both ways and use the correct length/offset mesures (Marxon provides elements for compatibility).

In the following text, the STL prefix `std::` will be omitted most of the time.

# C++ API
The whole c++ API is in the namespace `Marxon`. The C++ API is based on a single class `Definitions` which stores formats definitions. All elements are part of the *Definitions* except those in the *Types*, *Non-member* and *Exceptions* subsections.

## Types
The type *Text* is introduced as an alias for the type *list<TextPart>*.

The type *TextPart* is a structure with the following elements:
- `string text` The text processed by Marxon (JSON or YAML)
- `size_t offset` The offset of the text part in terms of utf8 elements (8 bits) in the document
- `size_t offset16` The offset of the text part in terms of utf16 elements (16 bits) in the document
- `size_t offset32` The offset of the text part in terms of utf32 elements (32 bits) in the document
- `size_t len` The length of the text part in terms of utf8 elements (8 bits) in the document
- `size_t len16` The length of the text part in terms of utf16 elements (16 bits) in the document
- `size_t len32` The length of the text part in terms of utf32 elements (32 bits) in the document
- `bool hasText` Whether the part contains text to be read by humans

## Constructors/Copy
```c++
Definitions(void)
```
Default constructor

```c++
Definitions(const Definitions& d)
```
Copy constructor

```c++
Definitions(const unordered_map<string,string>& defMap)
```
Construct definitions based on map *defMap*. Each element of *defMap* corresponds to a format definition, the key is the name of the format defined in the value (text of a YAML format definition).

```c++
Definitions(const string& defStr)
```
Construct definitions based on string *defStr*. *defStr* is the text of a YAML map. Each element of *defStr* corresponds to a format definition, the key is the name of the format defined in the value (text of a YAML format definition).

```c++
Definitions(const string& name, const string& def)
```
Construct definitions containing a unique format with name *name* and definition *def* (text of a YAML format definition).

```c++
Definitions(const string& def)
```
Construct definitions containing a unique format with name "§§only" and definition *def* (text of a YAML format definition). In case of name conflict, the new format replaces the old one.

```c++
Definitions& =(const Definitions& d)
```
Copy

## Add/remove formats to existing objects
```c++
void addDef(const unordered_map<string,string>& defMap)
```
Add formats based on map *defMap*. Each element of *defMap* corresponds to a format definition, the key is the name of the format defined in the value (text of a YAML format definition). In case of name conflict, the new format replaces the old one.

```c++
void addDef(const string& defStr)
```
Add formats based on string *defStr*. *defStr* is the text of a YAML map. Each element of *defStr* corresponds to a format definition, the key is the name of the format defined in the value (text of a YAML format definition). In case of name conflict, the new format replaces the old one.

```c++
void addDef(const string& def)
```
Add formats containing a unique format with name "§§only" and definition *def* (text of a YAML format definition). In case of name conflict, the new format replaces the old one.

```c++
void addDef(const string& name, const string& def)
```
Add formats containing a unique format with name *name* and definition *def* (text of a YAML format definition). In case of name conflict, the new format replaces the old one.

```c++
void reset(void)
```
Clear the object.

## Set/get parameters
```c++
void setTabLen(unsigned l)
```
Set the length of a tabulation in input. Tabulations are interpreted as defined in the Python 3 specification. The default length is 8 (old Unix standard).

```c++
unsigned getTabLen(void) const
```
Get the length of a tabulation in input.

```c++
void setLang(const string& l)
```
Set the default format (to use with inputs without format specified).

```c++
string getLang(void) const
```
Get the default format.

```c++
void resetContext(const string& l = /*default format*/)
```
Reset the default context for the given format *l*.

```c++
void setContext(const string& c, const string& l = /*default format*/)
```
Set the default context to *c* for the given format *l*.

```c++
string getContext(const string& l = /*default format*/) const
```
Get the default context for the given format *l*.

```c++
void setContext(const unordered_map<std::string,std::string>& c)
```
For each key-value pair in *c*, set the default context to value for the format key.

```c++
void replaceContext(const unordered_map<std::string,std::string>& c)
```
Use *c* a format-default context map.

```c++
unordered_map<std::string,std::string> getContexts(void) const
```
Get the format-default context map.

```c++
bool isSupported(const string& l) const
```
Test whether the format *l* is present in the definitions.

## Process input
```c++
string json(const string& doc, const string& l = /*default format*/, const string& c = /*default context*/) const
```
Process *doc* in format *l* starting in context *c* (JSON string output).

```c++
string yaml(const string& doc, const string& l = /*default format*/, const string& c = /*default context*/) const
```
Process *doc* in format *l* starting in context *c* (YAML string output).

```c++
string jsonContext(const string& doc, const string& c) const
```
Process *doc* in default format starting in context *c* (JSON string output).

```c++
string yamlContext(const string& doc, const string& c) const
```
Process *doc* in default format starting in context *c* (YAML string output).

```c++
Text jsonList(const string& doc, const string& l = /*default format*/, const string& c = /*default context*/) const
```
Process *doc* in format *l* starting in context *c* (JSON Text output).

```c++
Text yamlList(const string& doc, const string& l = /*default format*/, const string& c = /*default context*/) const
```
Process *doc* in format *l* starting in context *c* (YAML Text output).

```c++
Text jsonContextList(const string& doc, const string& c) const
```
Process *doc* in default format starting in context *c* (JSON Text output).

```c++
Text yamlContextList(const string& doc, const string& c) const
```
Process *doc* in default format starting in context *c* (YAML Text output).

## Non-member
```c++
string dataPath(void)
```
Return the installation path of Marxon's stock format definitions.

## Exceptions
Outside of c++ classical exceptions and dependencies exceptions (YAML-cpp, nlohmann::json, utf8cpp) Marxon may throw an exception of class `Exception` (`Marxon::Exception`). The class `Exception` publicly inherit the class `std::exception`. The member function `const char* what(void) const` returns a description of the error.

# Python API
Python 3 is **required**.

The Python API is the module `marxon`. The Python API is based on a single class `Definitions` which stores formats definitions. All elements are part of the *Definitions* except those in the *Types*, *Non-member* and *Exceptions* subsections.

## Types
The class *TextPart* is a structure with the following elements:
- `text #str` The text processed by Marxon (JSON or YAML)
- `offset #int` The offset of the text part in terms of utf8 elements (8 bits) in the document
- `offset16 #int` The offset of the text part in terms of utf16 elements (16 bits) in the document
- `offset32 #int` The offset of the text part in terms of utf32 elements (32 bits) in the document
- `len #int` The length of the text part in terms of utf8 elements (8 bits) in the document
- `len16 #int` The length of the text part in terms of utf16 elements (16 bits) in the document
- `len32 #int` The length of the text part in terms of utf32 elements (32 bits) in the document
- `hasText #bool` Whether the part contains text to be read by humans

## Constructors
```python
__init__():
```
Default constructor

```python
__init__(defs) #dict[str,str] ->|
```
Construct definitions based on map *defs*. Each element of *defs* corresponds to a format definition, the key is the name of the format defined in the value (text of a YAML format definition).

```python
__init__(defs) #str ->|
```
Construct definitions based on string *defs*. *defs* is the text of a YAML map. Each element of *defs* corresponds to a format definition, the key is the name of the format defined in the value (text of a YAML format definition).

```python
__init__(defs,name) #str -> str ->|
```
Construct definitions containing a unique format with name *name* and definition *defs* (text of a YAML format definition).

```python
__init__(defs) #str ->|
```
Construct definitions containing a unique format with name "§§only" and definition *defs* (text of a YAML format definition). In case of name conflict, the new format replaces the old one.

## Add/remove formats to existing objects
```python
addDef(defs) #dict[str,str] ->
```
Add formats based on map *defs*. Each element of *defs* corresponds to a format definition, the key is the name of the format defined in the value (text of a YAML format definition).

```python
addDef(defs) #str ->
```
Add formats based on string *defs*. *defs* is the text of a YAML map. Each element of *defs* corresponds to a format definition, the key is the name of the format defined in the value (text of a YAML format definition).

```python
addDef(defs,name) #str -> str ->
```
Add formats containing a unique format with name *name* and definition *defs* (text of a YAML format definition).

```python
addDef(defs) #str ->
```
Add formats containing a unique format with name "§§only" and definition *defs* (text of a YAML format definition). In case of name conflict, the new format replaces the old one.

```python
reset() #->
```
Clear the object.

## Set/get parameters
```python
setTabLen(l) #int ->
```
Set the length of a tabulation in input. Tabulations are interpreted as defined in the Python 3 specification. The default length is 8 (old Unix standard).

```python
getTabLen() #-> int
```
Get the length of a tabulation in input.

```python
setLang(l) #str ->
```
Set the default format (to use with inputs without format specified).

```python
getLang() #-> str
```
Get the default format.

```python
resetContext(l=__default_format) #(str) ->
```
Reset the default context for the given format *l*.

```python
setContext(self,c,l=__default_format): #str -> (str) ->
```
Set the default context to *c* for the given format *l*.

```python
getContext(l=__default_format) #(str) -> str
```
Get the default context for the given format *l*.

```python
replaceContext(c) #dict[str,str] ->
```
Use *c* a format-default context map.

```python
getContexts() #-> dict[str,str]
```
Get the format-default context map.

```python
isSupported(l) #str -> bool
```
Test whether the format *l* is present in the definitions.

## Process input
```python
json(doc,l=__default_format,c=__default_context[l]) #str -> (str) -> (str) -> str
```
Process *doc* in format *l* starting in context *c* (JSON string output).

```python
yaml(doc,l=__default_format,c=__default_context[l]) #str -> (str) -> (str) -> str
```
Process *doc* in format *l* starting in context *c* (YAML string output).

```python
jsonList(doc,l=__default_format,c=__default_context[l]) #str -> (str) -> (str) -> list[str]
```
Process *doc* in format *l* starting in context *c* (JSON Text output).

```python
yamlList(doc,l=__default_format,c=__default_context[l]) #str -> (str) -> (str) -> list[str]
```
Process *doc* in format *l* starting in context *c* (YAML Text output).

## Non-member
```python
dataPath() #-> str
```
Return the installation path of Marxon's stock format definitions.

## Exceptions
Exceptions thrown from c++ code are translated to Python exceptions by the pybind11 library.

Outside of c++ classical exceptions and dependencies exceptions (YAML-cpp, nlohmann::json, utf8cpp) Marxon may throw an exception of class `Exception` (`Marxon::Exception`). The class `Exception` publicly inherit the class `std::exception`. The member function `const char* what(void) const` returns a description of the error.

# C API
The C API is currently **untested** and not formally defined. It is mostly a C translation of the C++ API. See the code (src/include/marxon.h and src/interface/interface_c.c++) for details.
